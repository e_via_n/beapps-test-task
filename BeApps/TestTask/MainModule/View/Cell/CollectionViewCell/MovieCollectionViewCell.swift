//
//  MovieCollectionViewCell.swift
//  TestTask
//
//  Created by Evian on 19.02.2021.
//

import UIKit

protocol MovieCollectionViewCellDelegate: class {
  func didLikedMovie(movie: Movie)
}

class MovieCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var posterView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var likeButton: UIButton!
  
  weak var delegate: MovieCollectionViewCellDelegate?
  var movie: Movie!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  @IBAction func didLikeMovie(_ sender: Any) {
    if movie.isLiked == false {
      likeButton.setImage(UIImage(named: "heart-fill"), for: .normal)
    } else {
      likeButton.setImage(UIImage(named: "heart"), for: .normal)
    }
    
      delegate?.didLikedMovie(movie: movie)
  }
  
  func setupView() {
    self.titleLabel.text = self.movie.title
    
    DispatchQueue.global(qos: .userInteractive).async {
      let image = UIImage(data: self.movie.imgBlob!)
      
      DispatchQueue.main.async {
        self.posterView.image = image
        
        if self.movie.isLiked == true {
          self.likeButton.setImage(UIImage(named: "heart-fill"), for: .normal)
        } else {
          self.likeButton.setImage(UIImage(named: "heart"), for: .normal)
        }
      }
    }
  }
}


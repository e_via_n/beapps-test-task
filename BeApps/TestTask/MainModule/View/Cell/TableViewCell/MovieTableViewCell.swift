//
//  MovieTableViewCell.swift
//  TestTask
//
//  Created by Evian on 21.02.2021.
//

import UIKit

fileprivate struct Constants {
  static let insetsCount: CGFloat = 3
  static let inset: CGFloat = 10
  static let numberOfRows: CGFloat = 2 // number of rows on the screen
}

protocol MovieTableViewCellDelegate: class  {
  func didSelectMovie(movie: Movie)
  func didLikedMovie(movie: Movie)
}

class MovieTableViewCell: UITableViewCell {
  
  @IBOutlet weak var moviesCollection: UICollectionView!
  
  weak var delegate: MovieTableViewCellDelegate?
  lazy var movies = [Movie]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupCollectionView()
  }

  private func setupCollectionView() {
    moviesCollection.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MovieCell")
    moviesCollection.dataSource = self
    moviesCollection.delegate = self
  }
  
  func setupView() {
    self.moviesCollection.reloadData()
  }
}

//MARK: -UICollectionViewDataSource
extension MovieTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return movies.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
    cell.delegate = self
    cell.movie = movies[indexPath.row]
    cell.setupView()
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    delegate?.didSelectMovie(movie: movies[indexPath.row])
  }
}

//MARK: -UICollectionViewDelegateFlowLayout
extension MovieTableViewCell: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width = (collectionView.frame.width - Constants.insetsCount * Constants.inset) / Constants.numberOfRows
    let height = collectionView.frame.height
    return CGSize(width: width, height: height)
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    let inset = Constants.inset
    return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
  }
  
}

//MARK: -MovieCollectionViewCellDelegate
extension MovieTableViewCell: MovieCollectionViewCellDelegate {
  func didLikedMovie(movie: Movie) {
    delegate?.didLikedMovie(movie: movie)
  }
}


//
//  MainViewController.swift
//  TestTask
//
//  Created by Evian on 19.02.2021.
//

import UIKit

enum CellType: Int {
  case genreCell
  case moviesCell
}

fileprivate struct Constants {
  static let genreRowHeight: CGFloat = 44
  static let countCells: CGFloat = 3 // number of cells on the screen
  static let countOfSectionCells: Int = 2 // count of cells in one section (Genre title + MoviesCollection)
}

class MainViewController: UIViewController {
  
  var presenter: MainViewPresenterProtocol!
  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var moviesTableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    overrideUserInterfaceStyle = .dark
    
    moviesTableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
    moviesTableView.register(UINib(nibName: "GenreViewCell", bundle: nil), forCellReuseIdentifier: "GenreViewCell")
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    moviesTableView.reloadData()
  }
}

//MARK: -UITableViewDataSource
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Constants.countOfSectionCells
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return presenter.movies?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      heightForCell(.genreCell)
      let cell = tableView.dequeueReusableCell(withIdentifier: "GenreViewCell",for: indexPath) as! GenreViewCell
      cell.titleLabel.text = presenter.movies?[indexPath.section].genre
      return cell
      
    } else {
      heightForCell(.moviesCell)
      let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
      guard let genreMovies = presenter.movies?[indexPath.section].movies else { return MovieTableViewCell()}
      cell.delegate = self
      cell.movies = genreMovies
      cell.setupView()
      return cell
    }
  }
  
  private func heightForCell(_ type: CellType) {
    switch type {
    case .genreCell:
      moviesTableView.rowHeight = Constants.genreRowHeight
    case .moviesCell:
      moviesTableView.rowHeight = (view.frame.height - Constants.genreRowHeight * Constants.countCells ) / Constants.countCells
    }
  }
}

//MARK: -MainViewProtocol
extension MainViewController: MainViewProtocol {
  func success() {
    DispatchQueue.main.async {
      self.title = "Enjoy your movies"
      self.activityIndicator.stopAnimating()
      self.moviesTableView.reloadData()
    }
  }
}

//MARK: -MovieTableViewCellDelegate
extension MainViewController: MovieTableViewCellDelegate {
  func didSelectMovie(movie: Movie) {
    presenter.tapOnTheMovie(movie: movie)
  }
  
  func didLikedMovie(movie: Movie) {
    presenter.didLikeMovie(movie: movie)
  }
}

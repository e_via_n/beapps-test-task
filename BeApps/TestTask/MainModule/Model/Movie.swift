//
//  MovieDTO.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation

class ViewModel {
  var genre: String?
  var movies: [Movie]?
  
  init(genre: String, movies: [Movie]) {
    self.genre = genre
    self.movies = movies
  }
}

class Movie: NSObject, NSCoding {
   
  var id: Int?
  var title: String?
  var createdAt: String?
  var genre: String?
  var imgID: String?
  var imgBlob: Data? 
  var isLiked = false
  var languages: [String]?
  
  init(_ id: Int, _ title: String, _ createdAt: String, _ genre: String, _ imgId: String, _ languages: [String]) {
    self.id = id
    self.title = title
    self.createdAt = createdAt
    self.genre = genre
    self.imgID = imgId
    self.languages = languages
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(id, forKey: "id")
    coder.encode(title, forKey: "title")
    coder.encode(createdAt, forKey: "createdAt")
    coder.encode(genre, forKey: "genre")
    coder.encode(imgID, forKey: "imgID")
    coder.encode(imgBlob, forKey: "imgBlob")
    coder.encode(isLiked, forKey: "isLiked")
    coder.encode(languages, forKey: "languages")

  }
  
  required init?(coder: NSCoder) {
    id = coder.decodeObject(forKey: "id") as? Int
    title = coder.decodeObject(forKey: "title") as? String
    createdAt = coder.decodeObject(forKey: "createdAt") as? String
    genre = coder.decodeObject(forKey: "genre") as? String
    imgID = coder.decodeObject(forKey: "imgID") as? String
    imgBlob = coder.decodeObject(forKey: "imgBlob") as? Data
    isLiked = coder.decodeObject(forKey: "isLiked") as? Bool ?? false
    languages = coder.decodeObject(forKey: "languages") as? [String]
  }
}

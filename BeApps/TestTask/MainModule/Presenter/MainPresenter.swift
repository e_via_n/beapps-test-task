//
//  MainPresenter.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation

protocol MainViewProtocol: class {
  func success()
}

protocol MainViewPresenterProtocol: class {
  init(view: MainViewProtocol, movieDataManager: MovieDataManagerProtocol, router: RouterProtocol)
  
  func getMovies()
  func tapOnTheMovie(movie: Movie?)
  func didLikeMovie(movie: Movie)
  var movies: [ViewModel]? { get set }
}


final class MainPresenter: MainViewPresenterProtocol {
  
  //MARK: -Props
  weak var view : MainViewProtocol?
  
  private var router: RouterProtocol?
  private var movieDataManager: MovieDataManagerProtocol!
  
  var movies: [ViewModel]?

  
  required init(view: MainViewProtocol, movieDataManager: MovieDataManagerProtocol, router: RouterProtocol) {
    self.view = view
    self.router = router
    self.movieDataManager = movieDataManager
    getMovies()
  }
  
  //MARK: -Funcs
  func tapOnTheMovie(movie: Movie?) {
    router?.showDetail(movie: movie)
  }
  
  func didLikeMovie(movie: Movie) {
    FavoritesService.shared.handleFavorites(movie: movie)
  }
  
  func getMovies() {
    
    movieDataManager.getMovies{ [weak self] result in
      guard let self = self else { return }

      switch result {
      case .success(let movies):
        self.movies = movies
        self.view?.success()

      case .failure(let error):
        self.handleError(error)
      }
    }
  }
  
  private func handleError(_ error: Error) {
    DispatchQueue.main.async {
      self.router?.showErrorAlert(error.localizedDescription)
    }
  }
}



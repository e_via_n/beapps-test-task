//
//  DetailPresenter.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation

protocol DetailViewProtocol: class {
  func setMovie(movie: Movie?)
}

protocol DetailViewPresenterProtocol: class {
  init(view: DetailViewProtocol, movie: Movie?, router: RouterProtocol)
  func setMovie()
}

final class DetailViewPresenter: DetailViewPresenterProtocol {
  
  weak var view: DetailViewProtocol?
  var movie: Movie?
  private var router: RouterProtocol?
  
  required init(view: DetailViewProtocol, movie: Movie?, router: RouterProtocol) {
    self.view = view
    self.movie = movie
    self.router = router
  }
  
  func setMovie() {
    self.view?.setMovie(movie: movie)
  }
}

//
//  DetailViewController.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import UIKit

class DetailViewController: UIViewController {
  
  var presenter: DetailViewPresenterProtocol!
  
  @IBOutlet weak var posterImageView: UIImageView!
  @IBOutlet weak var movieTitle: UILabel!
  @IBOutlet weak var createdAt: UILabel!
  @IBOutlet weak var languages: UILabel!
  
  private let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter.setMovie()
    appDelegate.myOrientation = .landscape
    UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    appDelegate.myOrientation = .all
  }
}

extension DetailViewController: DetailViewProtocol {
  func setMovie(movie: Movie?) {
    self.createdAt.text = movie?.createdAt?.dateFormat()
    self.movieTitle.text = movie?.title
    guard let imgData = movie?.imgBlob else { return }
    let image = UIImage(data: imgData)
    self.posterImageView.image = image
    self.languages.text = movie?.languages?[0...].joined(separator: ", ")
  }
}

//
//  Extensions.swift
//  TestTask
//
//  Created by Evian on 24.02.2021.
//

import Foundation

extension String {
  func dateFormat() -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    guard let date = dateFormatter.date(from: self) else { return "Unknown date" }
    dateFormatter.dateFormat = "dd-MM-yy"
    return dateFormatter.string(from: date)
  }
}


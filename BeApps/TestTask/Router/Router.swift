//
//  Router.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import UIKit

protocol RouterMain {
  var navigationController: UINavigationController? { get set }
  var assemblyBuilder: AssemblyBuilderProtocol? { get set }
}

protocol RouterProtocol: RouterMain {
  func initialViewController()
  func showDetail(movie: Movie?)
  func showErrorAlert(_ errorDescription: String)
}

final class Router: RouterProtocol {
  
  var navigationController: UINavigationController?
  var assemblyBuilder: AssemblyBuilderProtocol?
  
  init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
    self.navigationController = navigationController
    navigationController.overrideUserInterfaceStyle = .dark
    self.assemblyBuilder = assemblyBuilder
  }
  
  func initialViewController() {
    if let navigationController = navigationController {
      guard let mainViewController = assemblyBuilder?.createMainModule(router: self) else { return }
      navigationController.viewControllers = [mainViewController]
    }
  }
  
  func showDetail(movie: Movie?) {
    if let navigationController = navigationController {
      guard let detailViewController = assemblyBuilder?.createDetailModule(movie: movie, router: self) else { return }
      navigationController.pushViewController(detailViewController, animated: true)
    }
  }
  
  func showErrorAlert(_ errorDescription: String) {
    if let navigationController = navigationController {
      let alert = UIAlertController(title: "Something went wrong, try again later...", message: errorDescription, preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
      navigationController.present(alert, animated: true, completion: nil)
    }
  }
}


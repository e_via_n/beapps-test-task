//
//  NetworkService.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import Moya

protocol NetworkServiceProtocol {
  init(parsingService: ParsingServiceProtocol)
  func getMovies(completion: @escaping(Result<[Movie]?, Error>) -> Void)
}

final class NetworkService: NetworkServiceProtocol {
  
  private let parsingService : ParsingServiceProtocol!
  private let provider = MoyaProvider<MovieService>()
  
  required init(parsingService: ParsingServiceProtocol) {
    self.parsingService = parsingService
  }
  
  /// Getting all movies data from internet
  func getMovies(completion: @escaping(Result<[Movie]?, Error>) -> Void) {
    
    provider.request(.getMovies) { event in
      do {
      let response = try event.get()
        self.parsingService.parseData(from: response) { result in
          do {
            let result = try result.get()
            completion(.success(result))
          } catch let error {
            completion(.failure(error))
          }
        }
      } catch let error {
        completion(.failure(error))
      }
    }
  }
}

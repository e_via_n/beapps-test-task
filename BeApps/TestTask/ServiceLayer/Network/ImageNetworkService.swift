//
//  ImageNetworkService.swift
//  TestTask
//
//  Created by Evian on 24.02.2021.
//

import Foundation
import Moya

protocol ImageNetworkServiceProtocol {
  func getImage(for id: String, completion: @escaping(Result<Response?, Error>) -> Void)
}

final class ImageNetworkService: ImageNetworkServiceProtocol {
  private let provider = MoyaProvider<MovieService>(plugins: [NetworkDataCachingPlugin(configuration: .default, inMemoryCapacity: .max, diskCapacity: .max, diskPath: .none)])

  /// Getting image for movie
  /// - Parameters:
  ///   - id: needs for create URL
  func getImage(for id: String, completion: @escaping(Result<Response?, Error>) -> Void) {
    provider.request(.getImage(for: id)) { event in
      do {
        let response = try event.get()
        completion(.success(response))
      } catch let error {
        completion(.failure(error))
      }
    }
  }
}

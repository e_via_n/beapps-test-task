//
//  MoyaCachePlugin.swift
//  TestTask
//
//  Created by Evian on 21.02.2021.
//

import Foundation
import Moya


protocol CachePolicyGettable {
  var cachePolicy: URLRequest.CachePolicy { get }
}

final class NetworkDataCachingPlugin: PluginType {
  
  init (configuration: URLSessionConfiguration, inMemoryCapacity: Int, diskCapacity: Int, diskPath: String?) {
    configuration.urlCache = URLCache(memoryCapacity: inMemoryCapacity, diskCapacity: diskCapacity, diskPath: diskPath)
  }
  
  func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
    if let cacheableTarget = target as? CachePolicyGettable {
      var mutableRequest = request
      mutableRequest.cachePolicy = cacheableTarget.cachePolicy
      return mutableRequest
    }
    return request
  }
}

extension MovieService: CachePolicyGettable {
  var cachePolicy: URLRequest.CachePolicy {
    switch self {
    case .getImage(for: _):
      print("returningCacheData")
      return .returnCacheDataElseLoad
    default:
      return .useProtocolCachePolicy
    }
  }
}

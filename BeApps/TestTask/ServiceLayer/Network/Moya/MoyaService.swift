//
//  MoyaService.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import Moya

enum MovieService {
  case getMovies
  case getImage(for: String)
}

extension MovieService: TargetType {
  
  var baseURL: URL { return URL(string: "https://www.signalmediacorp.com")! }
  var path: String {
    switch self {
    case .getMovies:
      return "api/main_page"
    case .getImage(let id):
      return "//b/c/\(id).jpg"
    }
  }
  var method: Moya.Method {
    return .get
  }
  var parameters: [String: Any]? {
    return nil
  }
  var parameterEncoding: ParameterEncoding {
    return URLEncoding.default
  }
  var sampleData: Data {
    return Data()
  }
  var headers: [String : String]? {
    return ["Content-type": "application/json"]
  }
  var task: Task {
    return .requestPlain
  }
}






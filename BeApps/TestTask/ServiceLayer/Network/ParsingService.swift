//
//  ParsingService.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import Moya

protocol ParsingServiceProtocol {
  func parseData(from response: Response, completion: @escaping(Result<[Movie]?, Error>) -> Void)
}

final class ParsingService: ParsingServiceProtocol {
  
  func parseData(from response: Response, completion: @escaping(Result<[Movie]?, Error>) -> Void) {
    var movies = [Movie]()
    
    DispatchQueue.global(qos: .userInteractive).async {
      let data = response.data
      var json: [String: [AnyObject]]!
      
      do {
        json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String: [AnyObject]]
      } catch {
        completion(.failure(error))
      }
      
      guard let content = json["content"] else { return }
      
      for movieGenre in 0..<content.count {
        guard let moviesArr = content[movieGenre]["content"] as? [AnyObject] else { return }
        
        for movie in 0..<moviesArr.count {
          guard let id = moviesArr[movie]["id"] as? Int,
                let title = moviesArr[movie]["title"] as? String,
                let cover = moviesArr[movie]["cover"] as? [String:AnyObject],
                let genreObj = moviesArr[movie]["genre"] as? [String:AnyObject],
                let languagesObjArr = moviesArr[movie]["languages"] as? [AnyObject],
                let imgId = cover["id"] as? String,
                let genre = genreObj["title"] as? String,
                let createdAt = moviesArr[movie]["created_at"] as? String
          else { return }
          let allLanguages = languagesObjArr.compactMap { $0["short"] as? String }
          
          let movie = Movie(id, title, createdAt, genre, imgId, allLanguages)
          movies.append(movie)
        }
      }
      completion(.success(movies))
    }
  }
}

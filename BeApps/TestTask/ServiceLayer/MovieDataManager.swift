//
//  MovieDataManager.swift
//  TestTask
//
//  Created by Evian on 18.02.2021.
//

import Foundation
import Moya

protocol MovieDataManagerProtocol: class {
  init(networkService: NetworkServiceProtocol, dataSetupService: DataSetupServiceProtocol)
  func getMovies(completion: @escaping (Result<[ViewModel]?, Error>) -> Void)
}

final class MovieDataManager: MovieDataManagerProtocol {
  
  //MARK: -Props
  private var networkService: NetworkServiceProtocol!
  private var dataSetupService: DataSetupServiceProtocol!
  
  //MARK: -Init
  required init(networkService: NetworkServiceProtocol, dataSetupService: DataSetupServiceProtocol) {
    self.networkService = networkService
    self.dataSetupService = dataSetupService
  }
  
  //MARK: -Funcs
  func getMovies(completion: @escaping (Result<[ViewModel]?, Error>) -> Void) {
    networkService.getMovies { [weak self] result in
      guard let self = self else { return }
      
      do {
        let movies = try result.get()
        self.dataSetupService.setupMovies(movies!) { result in
          do {
            let sortedMovies = try result.get()
            completion(.success(sortedMovies))
          } catch let error {
            print(error)
            completion(.failure(error))
          }
        }
      } catch let error {
        print(error)
        completion(.failure(error))
      }
    }
  }
}

//
//  DataCompareService.swift
//  TestTask
//
//  Created by Evian on 18.02.2021.
//

import Foundation

protocol DataSetupServiceProtocol {
  init(imageService: ImageServiceProtocol, filterService: FilterServiceProtocol)
  func setupMovies(_ movies: [Movie], completion: @escaping(Result<[ViewModel]?, Error>) -> Void)
}

final class DataSetupService: DataSetupServiceProtocol {
  
  private var imageService: ImageServiceProtocol!
  private var filterService: FilterServiceProtocol!
  
  private var moviesArr = [Movie]()
  
  required init(imageService: ImageServiceProtocol, filterService: FilterServiceProtocol) {
    self.imageService = imageService
    self.filterService = filterService
  }
  
  public func setupMovies(_ movies: [Movie], completion: @escaping(Result<[ViewModel]?, Error>) -> Void) {
    for movie in movies {
      DispatchQueue.global().async {
        self.imageService.setImage(for: movie) { [weak self] result in
          guard let self = self else { return }
          
          do {
            var movie = try result.get()
            movie = self.setFavorites(movie!)
            
            self.moviesArr.append(movie!)
            guard self.moviesArr.count == movies.count else { return }
            
            DispatchQueue.global().async {
              let sortedMovies = self.filterByGenre(self.moviesArr)
              completion(.success(sortedMovies))
            }
          } catch let error {
            completion(.failure(error))
          }
        }
      }
    }
  }
}

//MARK: -Helper funcs
extension DataSetupService {
  private func filterByGenre(_ movies: [Movie]) -> [ViewModel]? {
    return self.filterService.filterByGenre(movies: movies)
  }
  
  private func setFavorites(_ movie: Movie) -> Movie {
    return FavoritesService.shared.setFavorites(movie: movie)
  }
}

//
//  FilterService.swift
//  TestTask
//
//  Created by Evian on 18.02.2021.
//

import Foundation

protocol FilterServiceProtocol {
  func filterByGenre(movies: [Movie]) -> [ViewModel]?
}

final class FilterService: FilterServiceProtocol {
  private var movies: [Movie]?
  
  func filterByGenre(movies: [Movie]) -> [ViewModel]? {
    
    self.movies = movies
    let dict = createDict()
    return createSortedViewModel(from: dict)
  }
}

//MARK: -Helper funcs
extension FilterService {
  private func createDict() -> [String : [Movie]]? {
    guard let movies = self.movies else { return nil }
    
    var genreSortedMovies = [String : [Movie]]()
    
    for movie in movies {
      guard let genre = movie.genre else { return nil }
      
      if genreSortedMovies[genre] != nil {
        
        genreSortedMovies[genre]?.append(movie)
      } else {
        genreSortedMovies[genre] = [movie]
      }
    }
    return genreSortedMovies
  }
  
  private func createSortedViewModel(from dict: [String : [Movie]]?) -> [ViewModel]? {
    guard let movies = dict else { return nil }
    let sortedMovies: Array = movies.sorted { $0.key > $1.key }
    return sortedMovies.map { ViewModel(genre: $0.key, movies: $0.value) }
  }
}


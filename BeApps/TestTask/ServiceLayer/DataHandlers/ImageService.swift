//
//  ImageService.swift
//  TestTask
//
//  Created by Evian on 19.02.2021.
//

import Foundation
import UIKit

protocol ImageServiceProtocol {
  init(imageNetworkService: ImageNetworkServiceProtocol)
  func setImage(for movie: Movie, completion: @escaping(Result<Movie?, Error>) -> Void)
}

final class ImageService: ImageServiceProtocol {
  
  private var imageNetworkService: ImageNetworkServiceProtocol!
  
  required init(imageNetworkService: ImageNetworkServiceProtocol) {
    self.imageNetworkService = imageNetworkService
  }
  
  func setImage(for movie: Movie, completion: @escaping(Result<Movie?, Error>) -> Void) {
    guard let imgID = movie.imgID else { return }
    
    self.imageNetworkService.getImage(for: imgID) { response in
      do {
        let response = try response.get()
        let data = response?.data
        movie.imgBlob = data
        completion(.success(movie))
        
      } catch let error {
        print("Image Request is failure")
        completion(.failure(error))
      }
    }
  }
}

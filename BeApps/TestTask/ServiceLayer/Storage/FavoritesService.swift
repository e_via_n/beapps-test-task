//
//  CoreDataService.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import UIKit

protocol FavoritesServiceProtocol {
  func handleFavorites(movie: Movie)
  func setFavorites(movie: Movie) -> Movie
}

final class FavoritesService: FavoritesServiceProtocol {
  
  static let shared = FavoritesService()
  
  private let defaults = UserDefaults.standard
  private let defaultsKey = "Favorites"
  
  private init() {}
  
  func setFavorites(movie: Movie) -> Movie {
    guard let favoriteMovies = loadFromFavorites() else { return movie }
    
    for favMovie in favoriteMovies {
      if favMovie.title == movie.title {
        movie.isLiked = true
      }
    }
    return movie
  }
  
  func handleFavorites(movie: Movie) {
    if movie.isLiked == true {
      movie.isLiked = false
      deleteFromFavorites(movie: movie)
    } else {
      movie.isLiked = true
      saveToFavorites(movie: movie)
    }
  }
  
  private func loadFromFavorites() -> [Movie]? {
    do {
      guard let moviesData  = UserDefaults.standard.object(forKey: defaultsKey) as? Data
      else { return nil }
      
      let favoriteMovies = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(moviesData) as! [Movie]
      return favoriteMovies
      
    } catch let error {
      print(error)
      return nil
    }
  }
  
  private func deleteFromFavorites(movie: Movie) {
    var favoriteMovies = loadFromFavorites() ?? []
    favoriteMovies.removeAll { $0.title == movie.title }
    defaults.removeObject(forKey: defaultsKey)
    saveData(moviesArr: favoriteMovies)
  }
  
  private func saveToFavorites(movie: Movie) {
    var favoriteMovies = loadFromFavorites() ?? []
    favoriteMovies.append(movie)
    saveData(moviesArr: favoriteMovies)
  }
  
  private func saveData(moviesArr: [Movie]) {
    do {
      let encodedData = try NSKeyedArchiver.archivedData(withRootObject: moviesArr, requiringSecureCoding: false)
      defaults.setValue(encodedData, forKey: defaultsKey)

    } catch let error {
      print(error)
    }
  }
}

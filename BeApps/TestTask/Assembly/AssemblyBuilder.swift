//
//  AssemblyBuilder.swift
//  TestTask
//
//  Created by Evian on 17.02.2021.
//

import Foundation
import UIKit

protocol AssemblyBuilderProtocol {
  func createMainModule(router: RouterProtocol) -> UIViewController
  func createDetailModule(movie: Movie?, router: RouterProtocol) -> UIViewController
}

final class AssemblyBuilder: AssemblyBuilderProtocol {
  
  func createMainModule(router: RouterProtocol) -> UIViewController {
    let movieDataManager = createMovieDataManager()
    let view = MainViewController()
    let presenter = MainPresenter(view: view, movieDataManager: movieDataManager, router: router)
    view.presenter = presenter
    return view
  }
  
  func createDetailModule(movie: Movie?, router: RouterProtocol) -> UIViewController {
    let view = DetailViewController()
    let presenter = DetailViewPresenter(view: view, movie: movie, router: router)
    view.presenter = presenter
    return view
  }
  
  //MARK: -Private funcs
  private func createMovieDataManager() -> MovieDataManager {
    let parsingService = ParsingService()
    let networkService = NetworkService(parsingService: parsingService)
    let dataSetupService = createDataSetupService()
    return MovieDataManager(networkService: networkService, dataSetupService: dataSetupService)
  }
  
  private func createDataSetupService() -> DataSetupService {
    let imageNetworkService = ImageNetworkService()
    let imageService = ImageService(imageNetworkService: imageNetworkService)
    let filterService = FilterService()
    return DataSetupService(imageService: imageService, filterService: filterService)
  }
}

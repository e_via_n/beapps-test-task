//
//  RouterTest.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import XCTest
@testable import TestTask

class MockNavigationController: UINavigationController {
  var presentedVC: UIViewController?
  
  override func pushViewController(_ viewController: UIViewController, animated: Bool) {
    self.presentedVC = viewController
    super.pushViewController(viewController, animated: animated)
  }
}


class RouterTest: XCTestCase {
  
  var router: RouterProtocol!
  var navigationController = MockNavigationController()
  let assembly = AssemblyBuilder()
  
  override func setUpWithError() throws {
    router = Router(navigationController: navigationController, assemblyBuilder: assembly)
  }
  
  override func tearDownWithError() throws {
    router = nil
  }
  
  func testRouter() {
    router.showDetail(movie: nil)
    let detailViewController = navigationController.presentedVC
    XCTAssertTrue(detailViewController is DetailViewController)
  }
}

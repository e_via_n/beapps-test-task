//
//  MainPresenterTest.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
import XCTest
import Moya

@testable import TestTask

class MainPresenterTest: XCTestCase {
  var view : MockView!
  var router: RouterProtocol!
  var presenter : MainPresenter!
  var movieDataManager: MovieDataManagerProtocol!
  var movies = [[ViewModel]]()
  
  override func setUpWithError() throws {
    let nav = UINavigationController()
    let assembly = AssemblyBuilder()
    router = Router(navigationController: nav, assemblyBuilder: assembly)
  }
  
  override func tearDownWithError() throws {
    view = nil
    movieDataManager = nil
    presenter = nil
  }
  
  func testGetSuccessMovies() {
    let genreMovies = [ViewModel(genre: "Foo", movies: [Movie(0, "bar", "11.11.11", "baz", "111", ["foo", "bar"])])]
    movies.append(genreMovies)
    
    view = MockView()
    movieDataManager = MockMovieDataManager(resultMovies: genreMovies)
    presenter = MainPresenter(view: view, movieDataManager: movieDataManager, router: router)
    
    var catchMovies: [ViewModel]?
    
    movieDataManager.getMovies { result in
      switch result {
      case .success(let movies):
        catchMovies = movies
      case .failure(let error):
        print(error)
      }
    }
    XCTAssertNotEqual(catchMovies?.count, 0)
    XCTAssertEqual(catchMovies?.count, movies.count)
  }
  
  func testGetFalureMovies() {
    let genreMovies = [ViewModel(genre: "Foo", movies: [Movie(0, "bar", "11.11.11", "baz", "111", ["foo", "bar"])])]
    movies.append(genreMovies)
    
    view = MockView()
    movieDataManager = MockMovieDataManager()
    presenter = MainPresenter(view: view, movieDataManager: movieDataManager, router: router)
    
    var catchErrors: Error?
    
    movieDataManager.getMovies { result in
      switch result {
      case .success(let movies):
        print(movies!)
      case .failure(let error):
        catchErrors = error
        print(error)
      }
    }
    XCTAssertNotNil(catchErrors)
  }
}

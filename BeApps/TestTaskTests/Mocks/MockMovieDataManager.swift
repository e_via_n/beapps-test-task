//
//  MockMovieDataManager.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockMovieDataManager: MovieDataManagerProtocol {
  private var networkService: NetworkServiceProtocol!
  private var dataSetupService: DataSetupServiceProtocol!
  
  var resultMovies: [ViewModel]?
  
  required init(networkService: NetworkServiceProtocol, dataSetupService: DataSetupServiceProtocol) {
    self.networkService = networkService
    self.dataSetupService = dataSetupService
  }
  
  init() { }
  
  convenience init(resultMovies: [ViewModel]?) {
    self.init()
    self.resultMovies = resultMovies
  }
  
  func getMovies(completion: @escaping (Result<[ViewModel]?, Error>) -> Void) {
    if let resultMovies = resultMovies {
      completion(.success(resultMovies))
    } else {
      let error = NSError(domain: "", code: 0, userInfo: nil)
      completion(.failure(error))
    }
  }
}

//
//  MockDataSetupService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockDataSetupService: DataSetupServiceProtocol {
  var imageService: ImageServiceProtocol!
  var filterService: FilterServiceProtocol!
  var favoritesService: FavoritesServiceProtocol!
  var moviesArr = [Movie]()
  
  required init(imageService: ImageServiceProtocol, filterService: FilterServiceProtocol) { }
  
  init() {}
  
  convenience init(imageService: ImageServiceProtocol, filterService: FilterServiceProtocol, sortedMovies: [ViewModel]?) {
    self.init()
    self.imageService = imageService
    self.filterService = filterService
  }
  
  func setupMovies(_ movies: [Movie], completion: @escaping (Result<[ViewModel]?, Error>) -> Void) {
    for movie in movies {
      imageService.setImage(for: movie) { result in
        do {
          var movie = try result.get()
          movie = self.favoritesService.setFavorites(movie: movie!)
          self.moviesArr.append(movie!)
          guard self.moviesArr.count == movies.count else { return }
          
          let sortedMovies = self.filterService.filterByGenre(movies: self.moviesArr)
          completion(.success(sortedMovies))
        } catch let error {
          completion(.failure(error))
        }
      }
    }
  }
}

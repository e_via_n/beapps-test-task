//
//  MockParsingService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
import Moya
@testable import TestTask

class MockParsingService: ParsingServiceProtocol {
  var movies: [Movie]?
  
  init() { }
  
  convenience init(movies: [Movie]?) {
    self.init()
    self.movies = movies
  }
  
  func parseData(from response: Response, completion: @escaping (Result<[Movie]?, Error>) -> Void) {
    if let movies = movies {
      completion(.success(movies))
    } else {
      let error = NSError(domain: "", code: 0, userInfo: nil)
      completion(.failure(error))
    }
  }
}

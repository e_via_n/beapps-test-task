//
//  MockFavoritesService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockFavortitesService: FavoritesServiceProtocol {
  
  var movie : Movie?
  
  init() {}
  
  convenience init(movie: Movie) {
    self.init()
    self.movie = movie
  }
  
  func handleFavorites(movie: Movie) { }
  
  func setFavorites(movie: Movie) -> Movie  {
    guard let favoriteMovies = loadFromFavorites() else { return movie }
    
    for favMovie in favoriteMovies {
      if favMovie.title == movie.title {
        movie.isLiked = true
      }
    }
    return movie
  }
  
  private func loadFromFavorites() -> [Movie]? {
    return [Movie(0, "Foo", "11.11.11", "bar", "111", ["foo", "bar"])]
  }
  
}

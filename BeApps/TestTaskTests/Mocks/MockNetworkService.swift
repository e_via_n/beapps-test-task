//
//  MockNetworkService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockNetworkService: NetworkServiceProtocol {
  
  var parsingService: ParsingServiceProtocol!
  var movies: [Movie]?
  
  required init(parsingService: ParsingServiceProtocol) { }
  
  init() {}
  
  convenience init(parsingService: ParsingServiceProtocol, movies: [Movie]?) {
    self.init()
    self.parsingService = parsingService
    self.movies = movies
  }
  
  func getMovies(completion: @escaping (Result<[Movie]?, Error>) -> Void) {
    if let movies = movies {
      completion(.success(movies))
    } else {
      let error = NSError(domain: "", code: 0, userInfo: nil)
      completion(.failure(error))
    }
  }
}

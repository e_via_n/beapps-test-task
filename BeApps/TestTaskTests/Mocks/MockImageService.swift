//
//  MockImageService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockImageService: ImageServiceProtocol {
  var resultMovie: Movie?
  
  required init(imageNetworkService: ImageNetworkServiceProtocol) { }
  
  init() {}
  
  convenience init(resultMovie: Movie?) {
    self.init()
    self.resultMovie = resultMovie
  }
  
  func setImage(for movie: Movie, completion: @escaping (Result<Movie?, Error>) -> Void) {
    if let movie = resultMovie {
      completion(.success(movie))
    } else {
      let error = NSError(domain: "", code: 0, userInfo: nil)
      completion(.failure(error))
    }
  }
}

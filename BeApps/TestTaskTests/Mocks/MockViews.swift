//
//  MockViews.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask


class MockView: MainViewProtocol {
  func success() {
    print(#function)
  }
}

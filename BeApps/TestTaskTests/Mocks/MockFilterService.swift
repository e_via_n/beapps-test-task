//
//  MockFilterService.swift
//  TestTaskTests
//
//  Created by Evian on 25.02.2021.
//

import Foundation
@testable import TestTask

class MockFilterService: FilterServiceProtocol {
  var movies: [Movie]?
  var resultMovies: [ViewModel]?
  
  convenience init(resultMovies: [ViewModel]?) {
    self.init()
    self.resultMovies = resultMovies
  }
  
  func filterByGenre(movies: [Movie]) -> [ViewModel]? {
    return resultMovies
  }

}
